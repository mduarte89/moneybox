﻿using Moneybox.App.DataAccess;
using Moneybox.App.Domain.Services;
using Moneybox.App.Domain.Services.Extensions;
using Moneybox.App.Features.Base;
using System;

namespace Moneybox.App.Features
{
    public class TransferMoney : AccountOperationsBase
    {
        public TransferMoney(IAccountRepository accountRepository, INotificationService notificationService) 
            : base (accountRepository, notificationService) { }

        public void Execute(Guid fromAccountId, Guid toAccountId, decimal amount)
        {
            ValidateTransferAmount(amount);
            ValidateAccountsForTransfer(fromAccountId, toAccountId);

            var fromAccount = TryGetAccountById(fromAccountId);
            var toAccount = TryGetAccountById(toAccountId);

            fromAccount.Withdraw(amount);
            toAccount.Deposit(amount);

            AccountRepository.Update(fromAccount);
            AccountRepository.Update(toAccount);

            // For this exercise Its not possible to infer if the entire Transfer operation is wrapped within a transaction, and if both the update and the notification would be rolled back in case of failure,
            // therefore changing the notification calls *after* the update as don't want the user to send user notifications in case there is an error when updating the account and the update doesn't take effect
            NotificationService.RaiseNotifications(fromAccount.DomainEvents);
            NotificationService.RaiseNotifications(toAccount.DomainEvents);
        }

        /// <summary>
        /// Validates the accounts for the transfer operation
        /// </summary>
        /// <param name="fromAccountId">Account to transfer the funds from</param>
        /// <param name="toAccountId">Account to transfer the funds to</param>
        private void ValidateAccountsForTransfer(Guid fromAccountId, Guid toAccountId)
        {
            if (fromAccountId == toAccountId)
            {
                throw new InvalidOperationException("Accounts to transfer and receive the amount are the same");
            }
        }
    }
}

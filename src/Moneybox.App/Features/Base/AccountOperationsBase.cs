﻿using Moneybox.App.DataAccess;
using Moneybox.App.Domain.Exceptions;
using Moneybox.App.Domain.Services;
using System;

namespace Moneybox.App.Features.Base
{
    /// <summary>
    /// Account Operation Base
    /// Operations which are common in account services
    public abstract class AccountOperationsBase
    {
        protected IAccountRepository AccountRepository;
        protected INotificationService NotificationService;

        protected AccountOperationsBase(IAccountRepository accountRepository, INotificationService notificationService)
        {
            AccountRepository = accountRepository;
            NotificationService = notificationService;
        }

        /// <summary>
        /// Tries to get account <see cref="Account"/> by its id
        /// </summary>
        /// <param name="accountId">Account Id</param>
        /// <returns>Returns the account <see cref="Account"/>if exists</returns>
        /// <exception cref="NotFoundException">If the account does not exist</exception>
        protected Account TryGetAccountById(Guid accountId)
        {
            // Given the Account Interface does not give away what result is expected in case of receiving a non existing account (either returns null or throws exception) - handling those scenarios here
            Account account;

            try
            {
                account = AccountRepository.GetAccountById(accountId);
            }
            catch (Exception ex)
            {
                throw new NotFoundException($"An exception occurred when trying to get account by id { accountId }", ex);
            }

            if (account == null)
            {
                throw new NotFoundException($"Cannot find account with id { accountId }");
            }

            return account;
        }

        /// <summary>
        /// Validates the amount to transfer
        /// </summary>
        /// <param name="amount">Amount to transfer</param>
        protected void ValidateTransferAmount(decimal amount)
        {
            if (amount <= 0m)
            {
                throw new InvalidOperationException("Operation amount cannot be <= 0");
            }
        }
    }
}

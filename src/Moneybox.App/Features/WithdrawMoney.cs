﻿using Moneybox.App.DataAccess;
using Moneybox.App.Domain.Exceptions;
using Moneybox.App.Domain.Services;
using Moneybox.App.Domain.Services.Extensions;
using Moneybox.App.Features.Base;
using System;

namespace Moneybox.App.Features
{
    public class WithdrawMoney : AccountOperationsBase
    {
        public WithdrawMoney(IAccountRepository accountRepository, INotificationService notificationService) 
            : base(accountRepository, notificationService) { }

        public void Execute(Guid fromAccountId, decimal amount)
        {
            ValidateTransferAmount(amount);
            var account = TryGetAccountById(fromAccountId);

            account.Withdraw(amount);

            AccountRepository.Update(account);

            NotificationService.RaiseNotifications(account.DomainEvents);
        }
    }
}

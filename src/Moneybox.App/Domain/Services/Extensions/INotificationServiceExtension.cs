﻿using Moneybox.App.Domain.Base.Interfaces;
using Moneybox.App.Domain.Events;
using System.Collections.Generic;
using System.Linq;

namespace Moneybox.App.Domain.Services.Extensions
{
    /// <summary>
    /// Notification service extension
    /// Still follows the guidelines of the problem and solution as technically the interface still remains intact, we just extend the service to improve the existing features
    /// </summary>
    public static class INotificationServiceExtension
    {
        public static void RaiseNotifications(this INotificationService notificationService, IReadOnlyCollection<IDomainEvent> events)
        {
            if(events == null || !events.Any())
            {
                return;
            }

            foreach (var @event in @events)
            {
                // Still not the best solution, given this class will have to be extended (modified) when new event types are created, but its just a side effect of the design of INotificationService
                // Although (IMO) its a very acceptible compromise given all new features will not need to be tied with specific methods in the notification service
                if(@event is FundsLowEvent) 
                {
                    notificationService.NotifyFundsLow(@event.UserEmail);
                }

                if(@event is ApproachingPayInLimitEvent)
                {
                    notificationService.NotifyApproachingPayInLimit(@event.UserEmail);
                }
            }
        }
    }
}

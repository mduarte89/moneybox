﻿using Moneybox.App.Domain.Base;
using System;

namespace Moneybox.App
{
    /// <summary>
    /// Aggregate root user
    /// </summary>
    public class User : Entity<Guid>
    {
        /// <summary>
        /// User constructor
        /// </summary>
        /// <param name="name">User name</param>
        /// <param name="email">User email</param>
        public User(string name, string email)
        {
            Name = name;
            Email = email;
        }

        /// <summary>
        /// User name
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// User email
        /// </summary>
        public string Email { get; private set; }

        /// <summary>
        /// Sets the name of the user
        /// </summary>
        /// <param name="name">User name</param>
        public void SetName (string name)
        {
            Name = name;
        }

        /// <summary>
        /// Sets the email of the user
        /// </summary>
        /// <param name="email">User email</param>
        public void SetEmail(string email)
        {
            Email = email;
        }
    }
}

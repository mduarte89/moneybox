﻿using System;
using System.Runtime.Serialization;

namespace Moneybox.App.Domain.Exceptions
{
    /// <summary>
    /// Not found exception
    /// </summary>
    [Serializable]
    public class NotFoundException : Exception
    {
        /// <summary>
        /// Not found exception constructor
        /// </summary>
        /// <param name="message">Exception message</param>
        public NotFoundException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Not found exception constructor
        /// </summary>
        /// <param name="message">Exception message</param>
        /// <param name="innerException">Inner Exception</param>
        public NotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Constructor needed for deserialization
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected NotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}

﻿using System.Collections.Generic;

namespace Moneybox.App.Domain.Base.Interfaces
{
    /// <summary>
    /// Aggregate Root interface for aggregate entities
    /// </summary>
    /// <typeparam name="TId"></typeparam>
    public interface IAggregateRoot<TId>
    {
        /// <summary>
        /// Root Id
        /// </summary>
        TId Id { get; }

        /// <summary>
        /// Domain Events
        /// </summary>
        IReadOnlyCollection<IDomainEvent> DomainEvents { get; }
    }
}

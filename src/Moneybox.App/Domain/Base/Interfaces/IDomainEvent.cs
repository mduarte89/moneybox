﻿namespace Moneybox.App.Domain.Base.Interfaces
{
    /// <summary>
    // Domain Event interface
    /// </summary>
    public interface IDomainEvent
    {
        string UserEmail { get; }
    }
}

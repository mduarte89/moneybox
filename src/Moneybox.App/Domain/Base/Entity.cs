﻿using Moneybox.App.Domain.Base.Interfaces;
using System;
using System.Collections.Generic;

namespace Moneybox.App.Domain.Base
{
    /// <summary>
    /// Aggregate root entity base
    /// </summary>
    /// <typeparam name="TId"></typeparam>
    public abstract class Entity<TId> : IAggregateRoot<TId>
    {
        /// <summary>
        /// Domain events
        /// </summary>
        private List<IDomainEvent> _domainEvents;

        /// <summary>
        /// Entity root Id
        /// </summary>
        public TId Id { get; protected set; }

        /// <summary>
        /// List of Domain events
        /// </summary>
        public IReadOnlyCollection<IDomainEvent> DomainEvents => _domainEvents.AsReadOnly();

        protected Entity()
        {
            _domainEvents = new List<IDomainEvent>();
        }

        /// <summary>
        /// Adds a domain event to the Entity
        /// </summary>
        /// <param name="event">Event to be added to the entity</param>
        protected void AddDomainEvent(IDomainEvent @event){
            _domainEvents.Add(@event);
        }
    }
}

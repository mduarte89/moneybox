﻿using Moneybox.App.Domain.Base;
using Moneybox.App.Domain.Events;
using System;

namespace Moneybox.App
{
    /// <summary>
    /// Aggregate Root Account
    /// </summary>
    public class Account : Entity<Guid>
    {
        public const decimal PayInLimit = 4000m;

        /// <summary>
        /// User Associated with the account
        /// </summary>
        public User User { get; private set; }

        /// <summary>
        /// Account Balance
        /// </summary>
        public decimal Balance { get; private set; }

        /// <summary>
        /// Total withdrawn from the account
        /// </summary>
        public decimal Withdrawn { get; private set; }

        /// <summary>
        /// Total paid in in the account
        /// </summary>
        public decimal PaidIn { get; private set; }

        /// <summary>
        /// Creates an instance of <see cref="Account"/>
        /// </summary>
        /// <param name="balance">Balance to add to the account when creating</param>
        /// <param name="user">User to associate the account with</param>
        public Account(decimal balance, User user) : this(user)
        {
            Id = Guid.NewGuid();
            Balance = balance;
            User = user;
        }

        /// <summary>
        /// Account Constructor
        /// One might want to create an account and don't deposit any funds in
        /// </summary>
        /// <param name="user">User to associate the account with</param>
        public Account(User user) : base()
        {
            Id = Guid.NewGuid();
            User = user;
            Balance = 0;
        }

        /// <summary>
        /// Withdraws the amount from the account
        /// </summary>
        /// <param name="amount">Amount to withdraw from the account</param>
        /// <exception cref="InvalidOperationException">Throws <see cref="InvalidOperationException"/>if amount is <= 0 </exception>
        public void Withdraw(decimal amount)
        {
            ValidateAmount(amount);

            if(Balance - amount < 0m)
            {
                throw new InvalidOperationException("Insufficient funds to perform operation");
            }

            Balance -= amount;
            Withdrawn += amount;

            if(Balance < 500m)
            {
                AddDomainEvent(new FundsLowEvent(User.Email));
            }
        }

        /// <summary>
        /// Deposits the amount in the account
        /// </summary>
        /// <param name="amount">Amount to deposit in the account</param>
        /// <exception cref="InvalidOperationException">Throws <see cref="InvalidOperationException"/>if amount is <= 0 </exception>
        public void Deposit(decimal amount)
        {
            ValidateAmount(amount);
            
            if (PaidIn + amount > PayInLimit)
            {
                throw new InvalidOperationException("Account pay in limit reached");
            }

            Balance += amount;
            PaidIn += amount;

            if (PayInLimit - PaidIn < 500m)
            {
                AddDomainEvent(new ApproachingPayInLimitEvent(User.Email));
            }
        }

        /// <summary>
        /// Validates the amount used in account operations
        /// </summary>
        /// <param name="amount">amount to validate</param>
        /// <exception cref="InvalidOperationException">Throws <see cref="InvalidOperationException"/>if amount is <= 0 </exception>
        private void ValidateAmount(decimal amount)
        {
            if(amount <= 0m)
            {
                throw new InvalidOperationException("Amount cannot be less or equal to 0");
            }
        }
    }
}

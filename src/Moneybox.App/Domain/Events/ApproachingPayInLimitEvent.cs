﻿using Moneybox.App.Domain.Base.Interfaces;

namespace Moneybox.App.Domain.Events
{
    /// <summary>
    /// Event to notify account approaching Pay in Limit
    /// </summary>
    public class ApproachingPayInLimitEvent : IDomainEvent
    {
        public ApproachingPayInLimitEvent(string userEmail)
        {
            UserEmail = userEmail;
        }

        public string UserEmail { get; private set; }
    }
}

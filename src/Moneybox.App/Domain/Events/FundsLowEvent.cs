﻿using Moneybox.App.Domain.Base.Interfaces;

namespace Moneybox.App.Domain.Events
{
    /// <summary>
    /// Event to notify account Funds low
    /// </summary>
    public class FundsLowEvent : IDomainEvent
    {
        public FundsLowEvent(string userEmail)
        {
            UserEmail = userEmail;
        }

        public string UserEmail { get; private set;}
    }
}

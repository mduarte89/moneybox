﻿using System;

namespace Moneybox.App.Tests.Helpers
{
    /// <summary>
    /// Class to create Entities for testing
    /// In a real situation would have used a Faker library such as either Autofixture or Bogus
    /// </summary>
    public static class Factory
    {
        /// <summary>
        /// Creates an Account
        /// </summary>
        /// <param name="balance">Balance to create the account</param>
        /// <param name="paidIn">PainIn amount (0) is not provided</param>
        /// <param name="user">User to associate the account with (a default will be created if one not provided)</param>
        /// <returns>Returns an account</returns>
        public static Account CreateAccount(decimal balance, decimal paidIn = 0, User user = null)
        {
            var userToCreate = user ?? CreateUser();
            var account = new Account(balance, userToCreate);
            
            account.GetType().GetProperty("PaidIn").SetValue(account, paidIn);

            return account;
        }

        /// <summary>
        /// Creates a new user
        /// </summary>
        /// <returns>Returns a User</returns>
        public static User CreateUser()
        {
            return new User("user name", $"{Guid.NewGuid()}@gmail.com");
        }
    }
}

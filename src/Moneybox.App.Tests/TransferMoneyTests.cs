using Moneybox.App.DataAccess;
using Moneybox.App.Domain.Exceptions;
using Moneybox.App.Domain.Services;
using Moneybox.App.Features;
using Moneybox.App.Tests.Helpers;
using Moq;
using NUnit.Framework;
using System;

namespace Moneybox.App.Tests
{
    public class Tests
    {
        private Mock<INotificationService> _notificationService;
        private Mock<IAccountRepository> _accountRepository;
        private TransferMoney _sut;

        [SetUp]
        public void Setup()
        {
            _notificationService = new Mock<INotificationService>();
            _accountRepository = new Mock<IAccountRepository>();
        }

        [Test]
        public void Execute_WhenFromAccountBalanceIsLessThan0_ShouldThrowInvalidOperationException()
        {
            // Arrange
            var accountFrom = Factory.CreateAccount(100m);
            var accountTo = Factory.CreateAccount(100m);
            _accountRepository.Setup(x => x.GetAccountById(accountFrom.Id)).Returns(accountFrom);
            _accountRepository.Setup(x => x.GetAccountById(accountTo.Id)).Returns(accountTo);

            _sut = new TransferMoney(_accountRepository.Object, _notificationService.Object);

            //Act && Assert
            Assert.Throws<InvalidOperationException>(() => _sut.Execute(accountFrom.Id, accountTo.Id, 101m), "Insufficient funds to perform operation");
        }

        [Test]
        public void Execute_WhenPayInSuperiorThanPayInLimit_ShouldThrowInvalidOperationException()
        {
            // Arrange
            var accountFrom = Factory.CreateAccount(100m);
            var accountTo = Factory.CreateAccount(Account.PayInLimit + 1);
            _accountRepository.Setup(x => x.GetAccountById(accountFrom.Id)).Returns(accountFrom);
            _accountRepository.Setup(x => x.GetAccountById(accountTo.Id)).Returns(accountTo);

            _sut = new TransferMoney(_accountRepository.Object, _notificationService.Object);

            //Act && Assert
            Assert.Throws<InvalidOperationException>(() => _sut.Execute(accountFrom.Id, accountTo.Id, 101m), "Account pay in limit reached");
        }

        [Test]
        [TestCase(0)]
        [TestCase(-1)]
        public void Execute_WhenAmountToTransferIsInvalid_ShouldInvalidOperationException(decimal amount)
        {
            // Arrange
            var accountFrom = Factory.CreateAccount(100m);
            var accountTo = Factory.CreateAccount(100m);
            _accountRepository.Setup(x => x.GetAccountById(accountFrom.Id)).Returns(accountFrom);
            _accountRepository.Setup(x => x.GetAccountById(accountTo.Id)).Returns(accountTo);

            _sut = new TransferMoney(_accountRepository.Object, _notificationService.Object);

            //Act && Assert
            Assert.Throws<InvalidOperationException>(() => _sut.Execute(accountFrom.Id, accountTo.Id, amount), "Operation amount cannot be <= 0");
        }

        [Test]
        public void Execute_WhenAccountFromDoesNotExist_ShouldThrowNotFoundException()
        {
            // Arrange
            var notFoundAccountUId = Guid.NewGuid();
            var account = Factory.CreateAccount(100m);
            _accountRepository.Setup(x => x.GetAccountById(account.Id)).Returns(account);

            _sut = new TransferMoney(_accountRepository.Object, _notificationService.Object);

            //Act && Assert
            Assert.Throws<NotFoundException>(() => _sut.Execute(notFoundAccountUId, account.Id, 100m), $"Cannot find account with id {notFoundAccountUId}");
        }

        [Test]
        public void Execute_WhenAccountToDoesNotExist_ShouldThrowNotFoundException()
        {
            // Arrange
            var notFoundAccountUId = Guid.NewGuid();
            var account = Factory.CreateAccount(100m);
            _accountRepository.Setup(x => x.GetAccountById(account.Id)).Returns(account);

            _sut = new TransferMoney(_accountRepository.Object, _notificationService.Object);

            //Act && Assert
            Assert.Throws<NotFoundException>(() => _sut.Execute(account.Id, notFoundAccountUId, 100m), $"Cannot find account with id {notFoundAccountUId}");
        }

        [Test]
        public void Execute_IfAccountServiceThrowsWhenGettingAccount_ShouldThrowNotFoundException()
        {
            // Arrange
            var notFoundAccountUId = Guid.NewGuid();
            var account = Factory.CreateAccount(100m);
            _accountRepository.Setup(x => x.GetAccountById(It.IsAny<Guid>())).Throws(new NotFoundException($"An exception occurred when trying to get account by id { notFoundAccountUId }"));

            _sut = new TransferMoney(_accountRepository.Object, _notificationService.Object);

            //Act && Assert
            Assert.Throws<NotFoundException>(() => _sut.Execute(notFoundAccountUId, account.Id, 100m), $"An exception occurred when trying to get account by id { notFoundAccountUId }");
        }

        [Test]
        public void Execute_WhenTransferingBetweenTheSameAccount_ShouldThrowInvalidOperationException()
        {
            // Arrange
            var account = Factory.CreateAccount(100m);
            _accountRepository.Setup(x => x.GetAccountById(account.Id)).Returns(account);

            _sut = new TransferMoney(_accountRepository.Object, _notificationService.Object);

            //Act && Assert
            Assert.Throws<InvalidOperationException>(() => _sut.Execute(account.Id, account.Id, 100m), "Accounts to transfer and receive the amount are the same");
        }

        [Test]
        public void Execute_WhenFromBalanceLessThan500_ShouldRaiseFundsLowNotificationAndUpdateAccounts()
        {
            // Arrange
            var fromAccount = Factory.CreateAccount(500m);
            var toAccount = Factory.CreateAccount(100m);

            _accountRepository.Setup(x => x.GetAccountById(fromAccount.Id)).Returns(fromAccount);
            _accountRepository.Setup(x => x.GetAccountById(toAccount.Id)).Returns(toAccount);

            _sut = new TransferMoney(_accountRepository.Object, _notificationService.Object);

            //Act
            _sut.Execute(fromAccount.Id, toAccount.Id, 50);

            // Asserts
            _notificationService.Verify(x => x.NotifyFundsLow(fromAccount.User.Email), Times.Once);
            _accountRepository.Verify(x => x.Update(fromAccount), Times.Once);
            _accountRepository.Verify(x => x.Update(toAccount), Times.Once);
            Assert.AreEqual(450m, fromAccount.Balance);
            Assert.AreEqual(50m, fromAccount.Withdrawn);
            Assert.AreEqual(150m, toAccount.Balance);
            Assert.AreEqual(50m, toAccount.PaidIn);
        }

        [Test]
        public void Execute_WhenFromBalanceIsNotLessThan500_ShouldUpdateAccountsAndNotRaiseFundsLowNotification()
        {
            // Arrange
            var fromAccount = Factory.CreateAccount(1000m);
            var toAccount = Factory.CreateAccount(100m);

            _accountRepository.Setup(x => x.GetAccountById(fromAccount.Id)).Returns(fromAccount);
            _accountRepository.Setup(x => x.GetAccountById(toAccount.Id)).Returns(toAccount);

            _sut = new TransferMoney(_accountRepository.Object, _notificationService.Object);

            //Act
            _sut.Execute(fromAccount.Id, toAccount.Id, 50);

            // Asserts
            _notificationService.Verify(x => x.NotifyFundsLow(fromAccount.User.Email), Times.Never);
            _accountRepository.Verify(x => x.Update(fromAccount), Times.Once);
            _accountRepository.Verify(x => x.Update(toAccount), Times.Once);
            Assert.AreEqual(950m, fromAccount.Balance);
            Assert.AreEqual(50m, fromAccount.Withdrawn);
            Assert.AreEqual(150m, toAccount.Balance);
            Assert.AreEqual(50m, toAccount.PaidIn);
        }

        [Test]
        public void Execute_WhenSumOfPaidInIsLessThan500_ShouldRaiseApproachingPayInLimitNotificationAndUpdateAccounts()
        {
            // Arrange
            var fromAccount = Factory.CreateAccount(5000m);
            var toAccount = Factory.CreateAccount(100m, 3500);

            _accountRepository.Setup(x => x.GetAccountById(fromAccount.Id)).Returns(fromAccount);
            _accountRepository.Setup(x => x.GetAccountById(toAccount.Id)).Returns(toAccount);

            _sut = new TransferMoney(_accountRepository.Object, _notificationService.Object);

            //Act
            _sut.Execute(fromAccount.Id, toAccount.Id, 500);

            // Asserts
            _notificationService.Verify(x => x.NotifyApproachingPayInLimit(toAccount.User.Email), Times.Once);
            _accountRepository.Verify(x => x.Update(fromAccount), Times.Once);
            _accountRepository.Verify(x => x.Update(toAccount), Times.Once);
            Assert.AreEqual(4500m, fromAccount.Balance);
            Assert.AreEqual(500m, fromAccount.Withdrawn);
            Assert.AreEqual(600m, toAccount.Balance);
            Assert.AreEqual(4000m, toAccount.PaidIn);
        }

        [Test]
        public void Execute_WhenSumOfPaidInIsMoreThan500_ShouldUpdateAccountsAndNotRaiseApproachingPayInLimitNotification()
        {
            // Arrange
            var fromAccount = Factory.CreateAccount(100m);
            var toAccount = Factory.CreateAccount(100m);

            _accountRepository.Setup(x => x.GetAccountById(fromAccount.Id)).Returns(fromAccount);
            _accountRepository.Setup(x => x.GetAccountById(toAccount.Id)).Returns(toAccount);

            _sut = new TransferMoney(_accountRepository.Object, _notificationService.Object);

            //Act
            _sut.Execute(fromAccount.Id, toAccount.Id, 100);

            // Asserts
            _notificationService.Verify(x => x.NotifyApproachingPayInLimit(toAccount.User.Email), Times.Never);
            _accountRepository.Verify(x => x.Update(fromAccount), Times.Once);
            _accountRepository.Verify(x => x.Update(toAccount), Times.Once);
            Assert.AreEqual(0m, fromAccount.Balance);
            Assert.AreEqual(100m, fromAccount.Withdrawn);
            Assert.AreEqual(200m, toAccount.Balance);
            Assert.AreEqual(100m, toAccount.PaidIn);
        }
    }
}
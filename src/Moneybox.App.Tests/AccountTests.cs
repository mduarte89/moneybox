﻿using Moneybox.App.Tests.Helpers;
using NUnit.Framework;
using System;

namespace Moneybox.App.Tests
{
    public class AccountTests
    {
        private Account _sut;
        
        [Test]
        public void Create_CanCreateAccountWithUserOnly_ShouldCreateAccountWith0Balance()
        {
            // Act
            _sut = new Account(Factory.CreateUser());

            // Assert
            Assert.AreEqual(0, _sut.Balance);
            Assert.IsNotNull(_sut.User);
            Assert.IsNotNull(_sut.DomainEvents);
        }

        [Test]
        public void Create_CanCreateAccountWithUserAndBalance_ShouldCreateAccountWithUserAndBalance()
        {
            // Act a
            _sut = new Account(10m, Factory.CreateUser());

            // Assert
            Assert.AreEqual(10m, _sut.Balance);
            Assert.IsNotNull(_sut.User);
            Assert.IsNotNull(_sut.DomainEvents);
        }

        [Test]
        [TestCase(0)]
        [TestCase(-1)]
        public void Withdraw_WhenProvidingInvalidAmount_ShouldInvalidOperationException(decimal amount)
        {
            //Arrange
            _sut = new Account(10m, Factory.CreateUser());

            // Act and Assert
            Assert.Throws<InvalidOperationException>(() => _sut.Withdraw(amount), "Operation amount cannot be <= 0");
        }

        [Test]
        public void Withdraw_WhenAmountMoreThanBalance_ShouldInvalidOperationException()
        {
            // Arrange
            _sut = new Account(10m, Factory.CreateUser());

            // Act and Assert
            Assert.Throws<InvalidOperationException>(() => _sut.Withdraw(11m), "Insufficient funds to perform operation");
        }

        [Test]
        public void Withdraw_WhenBalanceMoreThan500AfterWithdrawn_ShouldWithdrawAndNotAddDomainEvent()
        {
            // Arrange
            _sut = new Account(600m, Factory.CreateUser());

            // Act
            _sut.Withdraw(10m);

            // Assert
            Assert.AreEqual(590m, _sut.Balance);
            Assert.AreEqual(10m, _sut.Withdrawn);
            Assert.IsEmpty(_sut.DomainEvents);
        }

        [Test]
        public void Withdraw_WhenBalanceLessThan500AfterWithdrawn_ShouldWithdrawAndAddDomainEvent()
        {
            // Arrange
            _sut = new Account(600m, Factory.CreateUser());

            // Act
            _sut.Withdraw(101m);

            // Assert
            Assert.AreEqual(499m, _sut.Balance);
            Assert.AreEqual(101m, _sut.Withdrawn);
            Assert.AreEqual(1, _sut.DomainEvents.Count);
        }

        [Test]
        [TestCase(0)]
        [TestCase(-1)]
        public void Deposit_WhenProvidingInvalidAmount_ShouldInvalidOperationException(decimal amount)
        {
            //Arrange
            _sut = new Account(10m, Factory.CreateUser());

            // Act and Assert
            Assert.Throws<InvalidOperationException>(() => _sut.Deposit(amount), "Operation amount cannot be <= 0");
        }

        [Test]
        public void Deposit_WhenPaidInPlusAmountMoreThanPayInLimit_ShouldInvalidOperationException()
        {
            // Arrange
            _sut = new Account(10m, Factory.CreateUser());
            _sut.GetType().GetProperty("PaidIn").SetValue(_sut, 3000m);

            // Act and Assert
            Assert.Throws<InvalidOperationException>(() => _sut.Deposit(1001m), "Account pay in limit reached");
        }

        [Test]
        public void Deposit_WhenPaidLimitIsMoreThan500_ShouldDepositAndNotAddDomainEvent()
        {
            // Arrange
            _sut = new Account(1000m, Factory.CreateUser());

            // Act
            _sut.Deposit(100m);

            // Assert
            Assert.AreEqual(1100m, _sut.Balance);
            Assert.AreEqual(100m, _sut.PaidIn);
            Assert.IsEmpty(_sut.DomainEvents);
        }

        [Test]
        public void Deposit_WhenPaidLimitIsIsLessThan500AfterDeposit_ShouldWithdrawAndAddDomainEvent()
        {
            // Arrange
            _sut = new Account(1000m, Factory.CreateUser());
            _sut.GetType().GetProperty("PaidIn").SetValue(_sut, 3000m);

            // Act
            _sut.Deposit(501m);

            // Assert
            Assert.AreEqual(1501m, _sut.Balance);
            Assert.AreEqual(3501m, _sut.PaidIn);
            Assert.AreEqual(1, _sut.DomainEvents.Count);
        }
    }
}

﻿using Moneybox.App.Domain.Base.Interfaces;
using Moneybox.App.Domain.Events;
using Moneybox.App.Domain.Services;
using Moneybox.App.Domain.Services.Extensions;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace Moneybox.App.Tests.Services.Extensions
{
    public class INotificationServiceExtensionsTests
    {
        private Mock<INotificationService> _notificationService;

        [SetUp]
        public void Setup()
        {
            _notificationService = new Mock<INotificationService>();
        }

        [Test]
        public void RaiseNotifications_WhenNoEventDomain_ShouldNotRaiseEvents()
        {
            // Arrange
            var emptyCollection = new List<IDomainEvent>().AsReadOnly();

            // Act
            _notificationService.Object.RaiseNotifications(emptyCollection);

            // Assert
            _notificationService.Verify(x => x.NotifyFundsLow(It.IsAny<string>()), Times.Never);
            _notificationService.Verify(x => x.NotifyApproachingPayInLimit(It.IsAny<string>()), Times.Never);
        }


        [Test]
        public void RaiseNotifications_WhenNullDomainEvents_ShouldNotRaiseEvents()
        {
            // Arrange
            IReadOnlyCollection<IDomainEvent> emptyCollection = null;

            // Act
            _notificationService.Object.RaiseNotifications(emptyCollection);

            // Assert
            _notificationService.Verify(x => x.NotifyFundsLow(It.IsAny<string>()), Times.Never);
            _notificationService.Verify(x => x.NotifyApproachingPayInLimit(It.IsAny<string>()), Times.Never);
        }

        [Test]
        public void RaiseNotifications_WhenDomainEventIsOfTypeFundsLowEvent_ShouldCallNotifyFundsLowMethod()
        {
            // Arrange
            var emptyCollection = new List<IDomainEvent>()
            {
                new FundsLowEvent("SomeEmailAddress@gmail.com")
            }.AsReadOnly();

            // Act
            _notificationService.Object.RaiseNotifications(emptyCollection);

            // Assert
            _notificationService.Verify(x => x.NotifyFundsLow("SomeEmailAddress@gmail.com"), Times.Once);
            _notificationService.Verify(x => x.NotifyApproachingPayInLimit("SomeEmailAddress@gmail.com"), Times.Never);
        }

        [Test]
        public void RaiseNotifications_WhenDomainEventIsOfTypeApproachingPayInLimitEvent_ShouldCallNotifyApproachingPayInLimitethod()
        {
            // Arrange
            var emptyCollection = new List<IDomainEvent>()
            {
                new ApproachingPayInLimitEvent("SomeEmailAddress@gmail.com")
            }.AsReadOnly();

            // Act
            _notificationService.Object.RaiseNotifications(emptyCollection);

            // Assert
            _notificationService.Verify(x => x.NotifyFundsLow("SomeEmailAddress@gmail.com"), Times.Never);
            _notificationService.Verify(x => x.NotifyApproachingPayInLimit("SomeEmailAddress@gmail.com"), Times.Once);
        }
    }
}

﻿using Moneybox.App.DataAccess;
using Moneybox.App.Domain.Exceptions;
using Moneybox.App.Domain.Services;
using Moneybox.App.Features;
using Moneybox.App.Tests.Helpers;
using Moq;
using NUnit.Framework;
using System;

namespace Moneybox.App.Tests
{
    class WithdrawMoneyTests
    {
        private Mock<INotificationService> _notificationService;
        private Mock<IAccountRepository> _accountRepository;
        private WithdrawMoney _sut;

        [SetUp]
        public void Setup()
        {
            _notificationService = new Mock<INotificationService>();
            _accountRepository = new Mock<IAccountRepository>();
        }

        [Test]
        public void Execute_WhenAccountBalanceIsLessThan0_ShouldThrowInvalidOperationException()
        {
            // Arrange
            var account = Factory.CreateAccount(100m);
            _accountRepository.Setup(x => x.GetAccountById(account.Id)).Returns(account);

            _sut = new WithdrawMoney(_accountRepository.Object, _notificationService.Object);

            //Act && Assert
            Assert.Throws<InvalidOperationException>(() => _sut.Execute(account.Id, 101m), "Insufficient funds to perform operation");
        }

        [Test]
        [TestCase(0)]
        [TestCase(-1)]
        public void Execute_WhenAmountToTransferIsInvalid_ShouldInvalidOperationException(decimal amount)
        {
            // Arrange
            var account = Factory.CreateAccount(100m);
            _accountRepository.Setup(x => x.GetAccountById(account.Id)).Returns(account);

            _sut = new WithdrawMoney(_accountRepository.Object, _notificationService.Object);

            //Act && Assert
            Assert.Throws<InvalidOperationException>(() => _sut.Execute(account.Id, amount), "Operation amount cannot be <= 0");
        }

        [Test]
        public void Execute_WhenAccountDoesNotExist_ShouldThrowNotFoundException()
        {
            // Arrange
            var notFoundAccountUId = Guid.NewGuid();
            var account = Factory.CreateAccount(100m);
            _accountRepository.Setup(x => x.GetAccountById(account.Id)).Returns(account);

            _sut = new WithdrawMoney(_accountRepository.Object, _notificationService.Object);

            //Act && Assert
            Assert.Throws<NotFoundException>(() => _sut.Execute(notFoundAccountUId, 100m), $"Cannot find account with id {notFoundAccountUId}");
        }

        [Test]
        public void Execute_IfAccountServiceThrowsWhenGettingAccount_ShouldThrowNotFoundException()
        {
            // Arrange
            var notFoundAccountUId = Guid.NewGuid();
            var account = Factory.CreateAccount(100m);
            _accountRepository.Setup(x => x.GetAccountById(It.IsAny<Guid>())).Throws(new NotFoundException($"An exception occurred when trying to get account by id { notFoundAccountUId }"));

            _sut = new WithdrawMoney(_accountRepository.Object, _notificationService.Object);

            //Act && Assert
            Assert.Throws<NotFoundException>(() => _sut.Execute(account.Id, 100m), $"An exception occurred when trying to get account by id { notFoundAccountUId }");
        }

        [Test]
        public void Execute_WhenFromBalanceLessThan500_ShouldRaiseFundsLowNotificationAndUpdateAccounts()
        {
            // Arrange
            var account = Factory.CreateAccount(100m);

            _accountRepository.Setup(x => x.GetAccountById(account.Id)).Returns(account);

            _sut = new WithdrawMoney(_accountRepository.Object, _notificationService.Object);

            //Act
            _sut.Execute(account.Id, 50);

            // Asserts
            _notificationService.Verify(x => x.NotifyFundsLow(account.User.Email), Times.Once);
            _accountRepository.Verify(x => x.Update(account), Times.Once);
            Assert.AreEqual(50m, account.Balance);
            Assert.AreEqual(50m, account.Withdrawn);
        }

        [Test]
        public void Execute_WhenBalanceIsNotLessThan500_ShouldUpdateAccountsAndNotRaiseFundsLowNotification()
        {
            // Arrange
            var account = Factory.CreateAccount(1000m);
            _accountRepository.Setup(x => x.GetAccountById(account.Id)).Returns(account);

            _sut = new WithdrawMoney(_accountRepository.Object, _notificationService.Object);

            //Act
            _sut.Execute(account.Id, 50);

            // Asserts
            _notificationService.Verify(x => x.NotifyFundsLow(account.User.Email), Times.Never);
            _accountRepository.Verify(x => x.Update(account), Times.Once);
            Assert.AreEqual(950m, account.Balance);
            Assert.AreEqual(50m, account.Withdrawn);
        }
    }
}
